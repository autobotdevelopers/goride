'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', ['ngRoute','LoginController'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/',{
            templateUrl: './LoginPage/LoginPage.html',
            controller:'loginController'
        });
    }]);
